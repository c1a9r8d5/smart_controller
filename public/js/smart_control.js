var dlb = "/smart_control";
//Onload.
$(function(){
	var dataLoc = dlb + "/device"
	SAJAX.getJSONA("{}",dataLoc,function(devices){
		$.each(devices,function(i,device){
			
 			SAJAX.getJSONA("{}",dlb + "/device/" + device.deviceId,function(deviceDet){
				deviceDet.meta = device;
				console.log(deviceDet);
				if(typeof deviceDet.components.main.switch != 'undefined')
					$("body").append($("<button>").text(device.label).attr({devid:device.deviceId,on:deviceDet.components.main.switch.switch.value})
							.click(function(){
								toggleSwitch(this);
							})
					);
				else if(typeof deviceDet.components.main.contactSensor != 'undefined'){
					$("body").append($("<button>").addClass("status").text(device.label).attr({devid:device.deviceId, disabled:true, contact:deviceDet.components.main.contactSensor.contact.value}));
				}
				else
					$("body").append($("<button>").addClass("status").text(device.label).attr({devid:device.deviceId, disabled:true}));
			});
 		});
	});
});
//Will toggle a switch.
function toggleSwitch(obj){
	var state = $(obj).attr("on");
	var devid = $(obj).attr("devid");
	var switchTo = state=="on"?"off":"on";
	SAJAX.getJSONA("{}",`${dlb}/device/${devid}/${switchTo}`,function(){$(obj).attr("on",switchTo)});
	
}