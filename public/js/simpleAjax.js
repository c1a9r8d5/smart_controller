/* 
	Requires Jquery

   DEFINE NAME SPACE SAJAX
*/

//First Lets make sure JSON is supported, if not lets support it.

var JSON = JSON || {};

// implement JSON.stringify serialization
JSON.stringify = JSON.stringify || function (obj) {

var t = typeof (obj);
if (t != "object" || obj === null) {

    // simple data type
    if (t == "string") obj = '"'+obj+'"';
    return String(obj);

}
else {

    // recurse array or object
    var n, v, json = [], arr = (obj && obj.constructor == Array);

    for (n in obj) {
        v = obj[n]; t = typeof(v);

        if (t == "string") v = '"'+v+'"';
        else if (t == "object" && v !== null) v = JSON.stringify(v);

        json.push((arr ? "" : '"' + n + '":') + String(v));
    }

    return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
}
};

// implement JSON.parse de-serialization
JSON.parse = JSON.parse || function (str) {
if (str === "") str = '""';
eval("var p=" + str + ";");
return p;
 };
 
 
var SAJAX = SAJAX || {};
SAJAX.isOnline = true;
SAJAX.AJAXClass = function(thePostToPass, theDataToPass, theItemToShow, theDocumentPath)
{
	this.theHTML="";
	this.postToPass=thePostToPass;
	this.isJSON=false;
	this.async=false;
	this.disaplyLoad=true;
	this.dataToPass=theDataToPass;
	this.itemToShow=theItemToShow;
	this.documentPath=theDocumentPath;
	this.allowOffline=false;
	this.finOffline = false;
	//console.log("Created AJAX Class:");
	//console.log(this);
}
SAJAX.AJAXClass.prototype.sendAjax = function()
{
	if(this.disaplyLoad==true)
		{
			$(".theLoader").show();
			$(".greyOut").show();
		}

	//Check if offline
	$.ajax({
		url: "offlineTest",
		type: 'POST',
		async: this.async,
		cache: false,
		timeout: 10000,
		data: dataSend,

		success : function(msg)
		{
			SAJAX.isOnline = true;
		},
		error : function(jqXHR, textStatus, errorThrown)
		{
			SAJAX.isOnline = false;
		}
	});
		if(!SAJAX.isOnline)
		{
			SAJAX.isOnline = false;
			SAJAX.notifyOffline();
		}
		else
			SAJAX.clearNotifyOffline();
	if(SAJAX.isOnline)
	{
		if(localStorage["SAJAX"] !== undefined && !this.finOffline)
			SAJAX.completeAJAX();
		var funcHTML="";
		
		if(this.postToPass==0)
		{
		
			$.ajax({
				url: this.documentPath,
				type: 'POST',
				async: this.async,
				cache: false,
				contentType:(this.isJSON?'application/json':'application/x-www-form-urlencoded; charset=UTF-8'),
				timeout: 10000,
				data: this.dataToPass,

				success : function(msg)
				{
					funcHTML=msg;
				},
				error : function(jqXHR, textStatus, errorThrown)
				{
					//SAJAX.displayMessage("Error Sending: "  + errorThrown,false);
					return false;
				}
			});
		}
		else
		{
			var dataSend = this.postToPass + "=" + this.dataToPass;

			$.ajax({
				url: this.documentPath,
				type: 'POST',
				async: this.async,
				contentType:(this.isJSON?'application/json':'application/x-www-form-urlencoded; charset=UTF-8'),
				cache: false,
				timeout: 10000,
				data: dataSend,

				success : function(msg)
				{
					funcHTML=msg;
				},
				error : function(jqXHR, textStatus, errorThrown)
				{
					//displayMessage("Error Sending: "  + errorThrown,false);
					this.theHTML = false;
				}
			});
		}
		if(this.disaplyLoad==true)
		{	
			$(".theLoader").hide();
			$(".greyOut").hide();
		}
		this.theHTML=funcHTML;

		return this.theHTML;
	}
	else if(this.allowOffline === true)
	{
		try
		{
			var stringArray = JSON.parse(localStorage["SAJAX"]);
		}
		catch(err)
		{
			var stringArray = new Array();
		}
		var pushArray = [this.postToPass, this.dataToPass, "", this.documentPath];
		stringArray.push(pushArray);
		stringArray.push($("#appendMe").val());
		localStorage["SAJAX"] = JSON.stringify(stringArray);
		return true;
	}
	else
	{
			SAJAX.displayMessage("This Action cannot be done offline. ",false);
	}

}
SAJAX.completeAJAX = function()
{
	try
	{
		var AJAXArray = JSON.parse(localStorage["SAJAX"]);
	}
	catch(err)
	{		
		alert("Failed To Parse: "+localStorage["SAJAX"]);
		localStorage.removeItem("SAJAX");
	
		return false;
	}
	AJAXArray.forEach(function(AJAX) 
	{
		if(AJAX)
		{
			var resendAJAX = new SAJAX.AJAXClass(AJAX[0], AJAX[1], AJAX[2], AJAX[3]);
			resendAJAX.finOffline = true;
			var result = resendAJAX.sendAjax();
			if(result === false)
				return false;
		}
	});
	localStorage.removeItem("SAJAX");
}
SAJAX.notifyOffline = function()
{
	//if($(".topNotification").length < 1)
	//$("body").prepend('<div class="topNotification" id="offlineNot">It appears you are not connected to Intranet or are offline. Some features will not be available.</div>');
}
SAJAX.clearNotifyOffline = function()
{
	$("#offlineNot").remove();
}
SAJAX.displayMessage = function(theMessage,persist,duration)
{
		if(duration == null)
			var duration = 3000;
		if($(".message").length==0)
			$("body").prepend('<p class="message"></p></div>');
		$(".message").stop(true,true).slideUp();
		$(".message").html(theMessage);
		$(".message").slideDown();
		if(!persist)
			$(".message").delay(duration).slideUp();
}

//returns JSON to the user
SAJAX.getJSON = function(obj,dataLoc)
{
	var getData = new SAJAX.AJAXClass(0,JSON.stringify(obj),"",dataLoc);
	getData.isJSON=true; //We are using JSON
	getData = (getData.sendAjax());
	return getData;
}
//used for the standard requests. Will error if anything is abnormal or if .success is false
SAJAX.stdJSON = function(data,dataLoc,async,callback)
{
	var reply = SAJAX.getJSONA(data,dataLoc);
	if(typeof reply != "undefined" && reply != "" && reply.success === true){
		return reply;
	}
	else{
		alert("Error Occured accessing data! See log for details");
		console.error({data:data,location:dataLoc,reply:reply});
		return false;
	}
};
//This will get json async and run the callback.
SAJAX.getJSONA = function(data,dataLoc,callback){
	$.ajax({
		url: dataLoc,
		type: 'POST',
		async: true,
		cache: false,
		contentType:'application/json',
		timeout: 10000,
		data: JSON.stringify(data),

		success : function(msg)
		{
			if(typeof msg == "object")
				callback(msg);
			else
				console.error("Error getting:" + JSON.stringify(data) + " To " + dataLoc + " " +" With Content: " + JSON.stringify(msg))

		},
		error : function(jqXHR, textStatus, errorThrown)
		{
			console.error("Error sending:" + JSON.stringify(data) + " To " + dataLoc + " " + errorThrown + " With Content: " + JSON.stringify(jqXHR));
			alert("There was an error with this request please try again. Error Information: "+jqXHR.statusText+"");
		}
	});

}