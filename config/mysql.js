mysql= require('mysql');

module.exports = function() {
	var mysql_db = mysql.createConnection({
		host: config.mysql_host,
		user: config.mysql_user,
		password: config.mysql_password
	}
};