/*MAIN CONFIG file Only for variables that won't change via development cycle */
var cycle = typeof process.env.NODE_ENV == 'undefined'?'development':process.env.NODE_ENV;
config = require('./env/' + cycle + '.js');

config.project_name = "Home Hub";

//Smart things Private API key.
config.st_api_key = "";
config.st_api_address = "https://api.smartthings.com/v1";
module.exports = config;