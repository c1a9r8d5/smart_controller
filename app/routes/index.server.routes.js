module.exports = function(app) {
    var index = require('../controllers/index.server.controller');
    var smart_control = require('../controllers/smart_control.server.controller');
    var tasks = require('../controllers/tasks.server.controller');
    app.get('/', index.render);
    app.get('/smart_control', smart_control.render);
    app.post('/smart_control/device', smart_control.listDevices);
    app.get('/smart_control/device', smart_control.listDevices);
    app.post('/smart_control/device/:id', smart_control.showDevice);
    app.get('/smart_control/device/:id', smart_control.showDevice);
    app.post('/smart_control/device/:id/:action', smart_control.controlDevice);
    app.post('/smart_control/location', smart_control.listLocations);
    app.post('/smart_control/location/:id', smart_control.showLocation);
    app.post('/smart_control/location/:id/scene', smart_control.listScenes);
    app.post('/tasks', tasks.render);
};