/* if(config.st_api_key = "")
	throw "API not set within config/config.js! This is not going to work.";
 *///Display our smart_control Page
exports.render = function(req, res) {
    res.render('smart_control', {
        pageTitle: "Smart Control",
        pageDesc: "This page will have an array of buttons to control things.",
        project_name: config.project_name,
		start_date: config.start_date,
		developer: config.developer
    })
};

//List all devices in JSON
exports.listDevices = function(req, res) {
	getDevices().then(function(devices){
		res.json(devices);
	}).catch(function(err){res.status(201).json(err)}); 
};

//List a single device in JSON
exports.showDevice = function(req, res) {
	getDeviceStatus(req.params.id).then(function(device){
		res.json(device);
	}).catch(function(err){res.json(err)}); 
};


//List a single location in JSON
exports.showLocation = function(req, res) {
	getLocation(req.params.id).then(function(location){
		res.json(location);
	}).catch(function(err){res.json(err)}); 
};


//Sends Action to device
exports.controlDevice = function(req, res) {
	switch(req.params.action){
		case 'on':
			var command = [{
				command: 'on',
				capability: 'switch',
				component: 'main',
				arguments: []
			}];
		break;
		case 'off':
			var command = [{
				command: 'off',
				capability: 'switch',
				component: 'main',
				arguments: []
			}];
		break;
		default:
			res.end([]);
		break;
	}
	actuate(req.params.id,command).then(function(device){
		res.json(device);
	}).catch(function(err){res.json(err)}); 
};

//List all locations in JSON
exports.listLocations = function(req, res) {
	getLocations().then(function(locations){
		res.json(locations);
	}).catch(function(err){res.json(err)}); 
};

//List all scenes in JSON
exports.listScenes = function(req, res) {
	getScenes().then(function(scenes){
		res.json(scenes);
	}).catch(function(err){res.json(err)}); 
};



const rp = require('request-promise');


//Get all the locations within ST
function getLocations(url) {
  return rp({
    method: 'GET',
    url: url || `${config.st_api_address}/locations`,
    json: true,
    headers: {
      'Authorization': 'Bearer: ' + config.st_api_key
    }
  }).then(function(response) {
   return response.items;
  })
  .catch(function(err) {
    console.log(`Error getting devices: ${err}`);
  });
}

//Get all the scenes within ST
function getScenes(url) {
  return rp({
    method: 'GET',
    url: url || `${config.st_api_address}/scenes`,
    json: true,
    headers: {
      'Authorization': 'Bearer: ' + config.st_api_key
    }
  }).then(function(response) {
   return response.items;
  })
  .catch(function(err) {
    console.log(`Error getting devices: ${err}`);
  });
}

/**
 * Returns a request-promise for the status of the specified locationId.
 *
 * @param {string} locationId - The ID of the device.
 *
 * @returns {Object} - The request-promise for this API call.
 */
function getLocation(locationId) {
  const options = {
    method: 'GET',
    url: `${config.st_api_address}/locations/${locationId}`,
    json: true,
    headers: {
      'Authorization': 'Bearer: ' + config.st_api_key
    }
  };
  return rp(options);
}



//Get all the devices within ST
function getDevices(capability, url, devicesAccum) {
  return rp({
    method: 'GET',
    url: url || `${config.st_api_address}/devices`,
    qs: capability ? {capability: capability} : {},
    json: true,
    headers: {
      'Authorization': 'Bearer: ' + config.st_api_key
    }
  }).then(function(response) {
    if (!devicesAccum) {
      devicesAccum = [];
    }
    devicesAccum = devicesAccum.concat(response.items);
    if (response._links.next) {
      return getDevices(capability, response._links.next.href, devicesAccum);
    }
    return devicesAccum;
  })
  .catch(function(err) {
    console.log(`Error getting devices: ${err}`);
  });
}

/**
 * Returns a request-promise for the status of the specified deviceId.
 *
 * @param {string} deviceId - The ID of the device.
 *
 * @returns {Object} - The request-promise for this API call.
 */
function getDeviceStatus(deviceId) {
  const options = {
    method: 'GET',
    url: `${config.st_api_address}/devices/${deviceId}/status`,
    json: true,
    headers: {
      'Authorization': 'Bearer: ' + config.st_api_key
    }
  };
  return rp(options);
}


/**
 * Configures and returns a request-promise to actuate a device using the
 * SmartThings API.
 *
 * @param {string} deviceID - The ID of the device.
 * @param {Array} commands - An array of commands to send to the device.
 *
 * @returns {Object} - The request-promise for this API request.
 */
function actuate(deviceID, commands) {
  const options = {
    method: 'POST',
    url: `${config.st_api_address}/devices/${deviceID}/commands`,
    json: true,
    headers: {
      'Authorization': 'Bearer: ' + config.st_api_key
    },
    body: commands
  };
  return rp(options);
}

